package com.example.demo.filter;

import com.example.demo.entity.Account;
import com.example.demo.entity.Category;
import com.example.demo.entity.Member;
import com.example.demo.entity.Article;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ObjectifyHelper implements ServletContextListener{
    public void contextInitialized(ServletContextEvent event) {
        ObjectifyService.register(Account.class);
        ObjectifyService.register(Member.class);
        ObjectifyService.register(Article.class);
        ObjectifyService.register(Category.class);
    }
}
