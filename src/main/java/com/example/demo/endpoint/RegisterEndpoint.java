package com.example.demo.endpoint;

import com.example.demo.dto.AccountDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.generate.Generate;
import com.example.demo.util.generate.JsonResponse;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;
import com.googlecode.objectify.Ref;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

public class RegisterEndpoint extends HttpServlet {
    Gson gson = new Gson();
    MemberModel model = new MemberModel();
    JsonResponse jsonResponse = new JsonResponse();
    Generate generate = new Generate();
    HashPassword hash = new HashPassword();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            BufferedReader reader = req.getReader();
            if (reader != null) {
                AccountDTO accountDTO = gson.fromJson(reader, AccountDTO.class);
                // validate data.
                HashMap<String, String> errors = accountDTO.validate();
                if (errors.size() == 0) {
                    if (checkExist(accountDTO.getEmail()) == true) {
                        String salt = generate.getSalt(10);
                        String hashPassword = hash.md5(accountDTO.getPassword() + salt);
                        String defaultAvatar = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/813b6ef2-c4e3-4216-b5e6-a2104cb6de0e/dbxvip4-82481e9f-278c-4085-8864-122ed6d4c462.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzgxM2I2ZWYyLWM0ZTMtNDIxNi1iNWU2LWEyMTA0Y2I2ZGUwZVwvZGJ4dmlwNC04MjQ4MWU5Zi0yNzhjLTQwODUtODg2NC0xMjJlZDZkNGM0NjIucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.C_AuGEuNEaoMK9H2Qc-pDg_26QLCvgln0r3Lw5PhGAo";
                        long mls = Calendar.getInstance().getTimeInMillis();

                        Account account = new Account();
                        account.setId(mls);
                        account.setUsername(accountDTO.getUsername());
                        account.setPassword(hashPassword);
                        account.setSalt(salt);
                        account.setAvatar(defaultAvatar);
                        account.setEmail(accountDTO.getEmail());
                        account.setStatus(Account.Status.ACTIVE);
                        account.setRole(Account.Role.MEMBER);
                        account.setCreatedAtMLS(mls);
                        account.setUpdatedAtMLS(mls);

                        Member member = new Member();
                        member.setId(mls);
                        member.setGender(Member.Gender.findByValue(generate.convertStringGenderToInt(accountDTO.getGender())));
                        member.setAddress(accountDTO.getAddress());
                        member.setPhone(accountDTO.getPhone());
                        member.setFullname(accountDTO.getFullname());

                        account.setRefMember(Ref.create(member));
                        member.setRefAccount(Ref.create(account));

                        boolean check = model.register(account, member);
                        if (check == true) {
                            resp.setStatus(HttpServletResponse.SC_CREATED);
                            resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_CREATED).setMessage(ApplicationConstant.REGISTER_SUCCESS).setData(null).toJsonString());
                        } else {
                            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(ApplicationConstant.REGISTER_FAIL).setData(null).toJsonString());
                        }
                    } else {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(ApplicationConstant.EMAIL_EXIST).setData(null).toJsonString());
                    }
                } else {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(errors.toString()).setData(null));
                }
            }
        } catch (Exception ex) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).setMessage(ex.getMessage()).setData(null).toJsonString());
        }
    }

    private boolean checkExist(String email) {
        Account account = model.login(email);
        if (account == null) {
            return true;
        } else {
            return false;
        }
    }
}
