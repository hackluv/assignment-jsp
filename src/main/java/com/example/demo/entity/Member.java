package com.example.demo.entity;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Load;

@Entity
public class Member {
    @Id
    private long id;
    private String fullname;
    private String address;
    private String phone;
    private int gender;
    @Load
    Ref<Account> refAccount;

    public Member() {
    }

    public enum Gender {
        MALE(1), FEMALE(0), OTHERS(2);
        int value;

        Gender(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Gender findByValue(int value) {
            for (Gender gender :
                    Gender.values()) {
                if (gender.getValue() == value) {
                    return gender;
                }
            }
            return null;
        }
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        if (gender == null) {
            gender = Gender.FEMALE;
        }
        this.gender = gender.getValue();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Ref<Account> getRefAccount() {
        return refAccount;
    }

    public void setRefAccount(Ref<Account> refAccount) {
        this.refAccount = refAccount;
    }
}
