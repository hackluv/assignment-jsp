package com.example.demo.model;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MemberModel {

    /**
     * Register Member
     * @param account
     * @param member
     */
    public boolean register(Account account, Member member){
        ofy().save().entity(account).now();
        ofy().save().entity(member).now();
        return true;
    }

    /**
     * Login Member
     * @param email
     * */
    public Account login(String email){
        Account account = ofy().load().type(Account.class).filter("email", email).first().now();
        return account;
    }

    /**
     * Get list Account;
     * */
    public List<Account> getListAccount(){
        List<Account> list = ofy().load().type(Account.class).list();
        return list;
    }

    /**
     * Update detail member
     * */

    /**
     * Delete Member
     * */

    /**
     * Change avatar member
     * */
}
