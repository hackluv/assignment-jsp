package com.example.demo.endpoint;

import com.example.demo.dto.AccountDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.generate.Generate;
import com.example.demo.util.generate.JsonResponse;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

public class LoginEndpoint extends HttpServlet {
    Gson gson = new Gson();
    MemberModel model = new MemberModel();
    JsonResponse jsonResponse = new JsonResponse();
    HashPassword hash = new HashPassword();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            BufferedReader reader = req.getReader();
            if (reader != null) {
                Account account = gson.fromJson(reader, Account.class);
                HashMap<String, String> errors = account.validate();
                if (errors.size() == 0) {
                    String email = account.getEmail();
                    String password = account.getPassword();

                    Account account1 = model.login(email);
                    Member member = account1.getRefMember().get();
                    AccountDTO accDTO = new AccountDTO(account1, member);

                    if (account1 != null) {
                        if (hash.md5(password + account1.getSalt()).equals(account1.getPassword())) {
                            if (Account.Status.findByValue(account1.getStatus()) == Account.Status.ACTIVE) {
                                resp.setStatus(HttpServletResponse.SC_OK);
                                resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_OK).setMessage(ApplicationConstant.LOGIN_SUCCESS).setData(accDTO).toJsonString());
                            } else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DEACTIVE) {
                                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(ApplicationConstant.LOGIN_PLEASE_ACTIVE).setData(null).toJsonString());
                            } else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DELETED) {
                                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(ApplicationConstant.LOGIN_BLOCK).setData(null).toJsonString());
                            }
                        } else {
                            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(ApplicationConstant.LOGIN_WRONG_PASSWORD).setData(null).toJsonString());
                        }
                    } else {
                        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_NOT_FOUND).setMessage(ApplicationConstant.LOGIN_WRONG_EMAIL).setData(null).toJsonString());
                    }
                } else {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST).setMessage(errors.toString()).setData(null).toJsonString());
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_NOT_FOUND).setMessage(ApplicationConstant.LOGIN_FAIL).setData(null).toJsonString());
            }
        } catch (Exception ex) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().print(jsonResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).setMessage(ex.getMessage()).setData(null).toJsonString());
        }
    }
}
