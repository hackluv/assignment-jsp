package com.example.demo.filter;

import com.example.demo.dto.AccountDTO;
import com.example.demo.util.generate.ApplicationConstant;
import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {
    Gson gson = new Gson();

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        HttpSession session = request.getSession();
        String loginURI = request.getContextPath() + "/login";
        String json = (String) session.getAttribute(ApplicationConstant.CURRENT_LOGGED_IN);
        AccountDTO accDTO = gson.fromJson(json, AccountDTO.class);

        if (accDTO != null && accDTO.getRole().equals("Admin") && accDTO.getStatus().equals("Active")) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }

    public void destroy() {

    }

}
