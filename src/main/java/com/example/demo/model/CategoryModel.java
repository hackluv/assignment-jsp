package com.example.demo.model;

import com.example.demo.entity.Category;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CategoryModel {

    public boolean save(Category category) {
        ofy().save().entity(category).now();
        return true;
    }

    public Category categoryById(long id){
        Category category = ofy().load().type(Category.class).id(id).now();
        return category;
    }

    public List<Category> getListCategory(){
        List<Category> list = ofy().load().type(Category.class).list();
        return list;
    }

    public boolean updateCategory(Category category){
        ofy().defer().save().entity(category);
        return true;
    }

    public boolean deleteCategory(Category category){
        ofy().defer().save().entity(category);
        return true;
    }
}
