package com.example.demo.entity;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
public class Article {
    @Id
    private long id;
    @Index
    private String title;
    private String content;
    @Index
    private long createdAtMLS;
    @Index
    private long updatedAtMLS;
    @Index
    private long deletedAtMLS;
    @Index
    private int status;
    @Load
    Ref<Category> refCategory;
    @Load
    Ref<Member> refMember;

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Article.Status findByValue(int value) {
            for (Article.Status status :
                    Article.Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Article.Status status) {
        if (status == null) {
            status = Article.Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreatedAtMLS() {
        return createdAtMLS;
    }

    public void setCreatedAtMLS(long createdAtMLS) {
        this.createdAtMLS = createdAtMLS;
    }

    public long getUpdatedAtMLS() {
        return updatedAtMLS;
    }

    public void setUpdatedAtMLS(long updatedAtMLS) {
        this.updatedAtMLS = updatedAtMLS;
    }

    public long getDeletedAtMLS() {
        return deletedAtMLS;
    }

    public void setDeletedAtMLS(long deletedAtMLS) {
        this.deletedAtMLS = deletedAtMLS;
    }

    public Ref<Category> getRefCategory() {
        return refCategory;
    }

    public void setRefCategory(Ref<Category> refCategory) {
        this.refCategory = refCategory;
    }

    public Ref<Member> getRefMember() {
        return refMember;
    }

    public void setRefMember(Ref<Member> refMember) {
        this.refMember = refMember;
    }
}
