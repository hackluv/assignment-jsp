package com.example.demo.controller;

import com.example.demo.dto.AccountDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

public class LoginController extends HttpServlet {
    MemberModel model = new MemberModel();
    HashPassword hash = new HashPassword();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String referrer = req.getHeader("referer");
        System.out.println("Redirect từ trang: " + referrer);
        if (referrer == null) {
            referrer = "/login";
        }
        req.setAttribute("referer", referrer);
        req.getRequestDispatcher("/admin/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            Account account = new Account();
            account.setEmail(email);
            account.setPassword(password);
            HashMap<String, String> errors = account.validate();
            if (errors.size() == 0) {
                Account account1 = model.login(email);
                Member member = account1.getRefMember().get();
                AccountDTO accDTO = new AccountDTO(account1, member);

                if (account1 != null) {
                    if (hash.md5(password + account1.getSalt()).equals(account1.getPassword())) {
                        if (Account.Status.findByValue(account1.getStatus()) == Account.Status.ACTIVE) {
                            resp.setStatus(HttpServletResponse.SC_OK);
                            HttpSession session = req.getSession();
                            session.setAttribute(ApplicationConstant.CURRENT_LOGGED_IN, gson.toJson(accDTO));
                            resp.sendRedirect("/admin/dashboard");
                        } else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DEACTIVE) {
                            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ApplicationConstant.LOGIN_PLEASE_ACTIVE);
                        } else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DELETED) {
                            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ApplicationConstant.LOGIN_BLOCK);
                        }
                    } else {
                        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ApplicationConstant.LOGIN_WRONG_PASSWORD);
                    }
                } else {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND, ApplicationConstant.LOGIN_WRONG_EMAIL);
                }
            } else {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, errors.toString());
            }
        } catch (Exception ex) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            System.out.println("Error" + ex.getMessage());
        }
    }
}
