package com.example.demo.dto;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.util.generate.Generate;

import java.util.HashMap;

public class AccountDTO {
    private long id;
    private String username;
    private String email;
    private String role;
    private String password;
    private String salt;
    private String avatar;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String gender;
    private String fullname;
    private String address;
    private String phone;

    public AccountDTO() {
    }

    public AccountDTO(Account account, Member member) {
        this.id = account.getId();
        this.username = account.getUsername();
        this.email = account.getEmail();
        this.avatar = account.getAvatar();
        this.status = new Generate().convertIntStatusToString(account.getStatus());
        this.role = new Generate().convertIntRoleToString(account.getRole());
        this.gender = new Generate().convertIntGenderToString(member.getGender());
        this.fullname = member.getFullname();
        this.address = member.getAddress();
        this.phone = member.getPhone();
        this.createdAt = new Generate().convertMilToString(account.getCreatedAtMLS());
        this.updatedAt = new Generate().convertMilToString(account.getUpdatedAtMLS());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    // validate data
    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (username == null || username.isEmpty()) {
            errors.put("username", "username is required!");
        }
        if (email == null || email.isEmpty()) {
            errors.put("email", "email is required!");
        }
        if (fullname == null || fullname.isEmpty()) {
            errors.put("fullname", "fullname is required!");
        }
        if (address == null || address.isEmpty()) {
            errors.put("address", "address is required!");
        }
        if (phone == null || phone.isEmpty()) {
            errors.put("phone", "phone is required!");
        }
        if (password == null || password.isEmpty()) {
            errors.put("password", "password is required!");
        }
        if (gender == null || gender.isEmpty()) {
            errors.put("gender", "gender is required!");
        }
        return errors;
    }
}


